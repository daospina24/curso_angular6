import { v4 as uuidv4 } from 'uuid';

export class DestinoViaje {
  public id: number;
  public selected: boolean;
  servicios: string[];
  destinoId = uuidv4();
  constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) {

    this.servicios = ['Piscina', 'Desayuno', 'Transporte'];
  }

  isSelected(): boolean {
    return this.selected;
  }
  setSelected(value: boolean) {
    this.selected = value;
  }
  voteUp() {
    this.votes++;
  }
  voteDown() {
    this.votes--;
  }

}
