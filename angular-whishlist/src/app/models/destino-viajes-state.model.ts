import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { DestinoViaje } from './destino-viaje.model'

// ESTADO
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;

}


export const initializeDestinosViajesState = function () {
    return {
        items: [],
        loading: false,
        favorito: null
    }
}

export enum DestinoViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
}

export class NuevoDestinoAction implements Action {
    type = DestinoViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) { }
}

export class ElegidoFavoritoAction implements Action {
    type = DestinoViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) { }
}


export class VoteUPAction implements Action {
    type = DestinoViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) { }
}
export class VoteDownAction implements Action {
    type = DestinoViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) { }
}


export type DestinosViajesAction = NuevoDestinoAction | ElegidoFavoritoAction | VoteDownAction | VoteUPAction;



export function reduceDestinosViajes(state: DestinosViajesState, action: DestinosViajesAction): DestinosViajesState {
    switch (action.type) {
        case DestinoViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };

        }

        case DestinoViajesActionTypes.ELEGIDO_FAVORITO: {
            let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            let myfavorito = new DestinoViaje(fav.nombre, fav.imagenUrl);
            myfavorito.setSelected(true);
            fav = Object.assign({}, fav, myfavorito);


            return Object.assign({}, state, {
                items: state.items.map((item, index) => {
                    let value = false;
                    if (fav.id == item.id) {
                        value = true;
                    }
                    return Object.assign({}, item, {
                        selected: value
                    })
                }),
                favorito: Object.assign({}, fav, {
                    selected: true
                })
            })
        }

        case DestinoViajesActionTypes.VOTE_UP: {
            let d: DestinoViaje = (action as VoteUPAction).destino;
            let dest = new DestinoViaje(d.nombre, d.imagenUrl, d.votes);
            dest.voteUp()

            return Object.assign({}, state, {
                items: state.items.map((item, index) => {
                    return (d.id == item.id) ? Object.assign({}, item, dest) : item;
                })
            })

        }

        case DestinoViajesActionTypes.VOTE_DOWN: {
            let d: DestinoViaje = (action as VoteDownAction).destino;
            let dest = new DestinoViaje(d.nombre, d.imagenUrl, d.votes);
            dest.voteDown()

            return Object.assign({}, state, {
                items: state.items.map((item, index) => {
                    return (d.id == item.id) ? Object.assign({}, item, dest) : item;
                })
            })

        }
        default: return state;
    }
}

 
@Injectable({
    providedIn: 'root'
})
export class DestinosViajesEffects {

    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(public actions$: Actions) { }
}
