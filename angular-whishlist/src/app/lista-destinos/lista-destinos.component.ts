import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinoApiClient } from '../models/destino-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})

export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  listaDestinos;
  favorito: DestinoViaje;

  constructor(public destinoApiclient: DestinoApiClient, public store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.destinos = [];
    this.updates = [];


    this.store.select(state => {
      return state.destinos.favorito;
    })
      .subscribe(data => {
        if (data != null) {
          this.updates.push('Se ha eligido a ' + data.nombre);
          this.favorito = data;
        }
      });

    this.store.select(state => state.destinos.items).subscribe(items => this.listaDestinos = items);


  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    this.destinoApiclient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje, index: number) {

    this.destinoApiclient.elegir(d);
  }

  getAll() {
    return this.listaDestinos;
  }


  agregar(titulo: HTMLInputElement) {
    console.log(titulo.value);
  }

  guardar(nombre: string, url: string): boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    console.log(this.destinos);
    return false;
  }

  limpiar(nombre: HTMLInputElement, url: HTMLInputElement) {
    nombre.value = '';
    url.value = '';
  }




}
