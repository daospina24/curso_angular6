import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUPAction, VoteDownAction } from '../models/destino-viajes-state.model';
import { DestinoApiClient } from '../models/destino-api-client.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  @Output() clickedEliminar: EventEmitter<DestinoViaje>;

  constructor(public destinoApiclient: DestinoApiClient, private store: Store<AppState>) {
    console.log(this.destino);
    this.clicked = new EventEmitter();
    this.clickedEliminar = new EventEmitter();
  }

  ngOnInit(): void {
    this.destino.id = this.position;

  }

  ir(): boolean {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUPAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino))
    return false;
  }


}
